export type tDimentions = number

export type tLevel = 'supereasy' | 'easy' | 'medium' | 'hard' | 'veryhard'
export type tPlank = '' | '\\' | '/'
export type tPosition = 'top' | 'bottom' | 'left' | 'right'
export type tDirection = 'up' | 'down' | 'left' | 'right'
export type tLocation = 'side' | 'field'

export interface iLevel {
    level: tLevel
    dimentions: tDimentions
    plankCount: number
    score: number
    timeout: number
}
export interface iCellCoordinate {
    x: number
    y: number
}
export interface iCell {
    x: number
    y: number
    plank: tPlank
    orderNumber: number // Порядковый номер
    occupied: boolean
}

export interface iSide {
    x: number
    y: number
    number: number // порядковый номер
    line: number    // номер соответствующей строки или столбца
    position: tPosition
    direction: tDirection
    showTarget: boolean
}

export interface iTarget {
    position: number
    selected: boolean
}

export interface iPlank {
    x: number
    y: number
    newDirection: tDirection
}

export interface iField {
    field: iCell[][]
    side: [iSide[], iSide[], iSide[], iSide[]]
    sideArray: iSide[],
    sidesShouldInitialize: boolean
    startButtonShouldInitialize: boolean
    dimentions: tDimentions
    // Сколько всего досок
    plankCount: number
    // Если true - открываем все доски
    plankOpen: boolean
    startPosition: number
    endPosition: number
    target: iTarget
    // Массив планок
    planks: iPlank[]
    // Координаты стороны с номером 0
    zeroSideCoordinates: iBallCoordinates
    // Показывать или нет планки
    showPlanks: boolean
    // Показывать мяч
    showBall: boolean
}

export interface iBallCoordinates {
    x: number
    y: number
}
