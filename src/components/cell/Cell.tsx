import { useAppSelector } from '../../state/hooks'
import { getCellId } from '../../utils'

import { iCell, tPlank } from '../../interfaces'

import styles from './Cell.module.scss'


const getPlankImage = (plank: tPlank) => {
    switch (plank) {
        case '/': {
            // eslint-disable-next-line @next/next/no-img-element
            return <img src="/img/slash.png" className={styles.plank} alt=''></img>
        }
        case '\\': {
            // eslint-disable-next-line @next/next/no-img-element
            return <img src="/img/backslash.png" className={styles.plank} alt=''></img>
        }
        case '': {
            return null
        }
        default: {
            return null
        }
    }
}


export const Cell = (cell: iCell) => {
    const showPlanks = useAppSelector(state => state.field.showPlanks)

    return (
        <div className={styles.container} id={getCellId(cell.x, cell.y)} >
            {showPlanks ? getPlankImage(cell.plank) : ''}
        </div>
    )
}


