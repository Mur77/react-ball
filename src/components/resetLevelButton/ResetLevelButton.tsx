import { useState } from 'react'
import { useAppDispatch } from '../../state/hooks'
import { startButtonShouldInitialize } from '../../state/actions'

import styles from './ResetLevelButton.module.scss'

interface iResetLevelButton {
    clickCallback: () => void
}

export const ResetLevelButton: React.FC<iResetLevelButton> = ({ clickCallback }) => {
    const [pressed, setPressed] = useState(false)
    const dispatch = useAppDispatch()

    const handleMouseUpDown = (e: React.MouseEvent) => {
        if (e.type === 'mousedown') {
            setPressed(true)
        }
        if (e.type === 'mouseup') {
            setPressed(false)
        }
    }

    const handleMouseLeave = () => {
        setPressed(false)
    }

    const handleClick = () => {
        dispatch(startButtonShouldInitialize(true))
        // TODO: это можно сделать через redux
        clickCallback()
    }

    return (
        <div
            className={ `${styles.container} ${pressed ? styles.pressed : ''}` }
            onMouseDown={handleMouseUpDown}
            onMouseUp={handleMouseUpDown}
            onMouseLeave={handleMouseLeave}
            onClick={handleClick}
        >
            <div className={styles.innerContainer}>
                {'Reset Level'}
            </div>
        </div>
    )
}
