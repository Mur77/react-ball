import React, { useState, useEffect } from 'react'
import clsx from 'clsx'
import { useAppDispatch, useAppSelector } from '../../state/hooks'
import { targetSelected, animationStart, showPlanks, canSelectTargetAction } from '../../state/actions'
import { Ball } from '../ball/Ball'

import styles from './Side.module.scss'

interface iSide {
    position: number
    ball?: boolean
    initFieldCallback: () => void
}

export const Side: React.FC<iSide> = ({ position, ball=false, initFieldCallback }) => {
    const [showTargetState, setShowTargetState] = useState(false)

    const target = useAppSelector(state => state.field.target)
    const planksVisible = useAppSelector(state => state.field.showPlanks)
    const endPosition = useAppSelector(state => state.field.endPosition)
    const animationDone = useAppSelector(state => state.app.animationDone)
    const canSelectTarget = useAppSelector(state => state.app.canSelectTarget)
    const sidesShouldInitialize = useAppSelector(state => state.field.sidesShouldInitialize)
    const dispatch = useAppDispatch()

    // Обработчик выбора целевой клетки.
    const handleSelectTarget = () => {
        if (!planksVisible && canSelectTarget) {
            dispatch(targetSelected( { position, selected: true }))
            dispatch(showPlanks(true))
            dispatch(canSelectTargetAction(false))
            dispatch(animationStart(true))
        }
    }

    const handleMouseEnter = (e: React.MouseEvent) => {
        if (!target.selected) {
            if (e.type === 'mouseenter') {
                setShowTargetState(true)
            } else {
                setShowTargetState(false)
            }
        }
    }

    useEffect(() => {
        if (sidesShouldInitialize) {
            setShowTargetState(false)
        }
    }, [sidesShouldInitialize])

    const containerClass = clsx(styles.container, {
        [styles.hit]: animationDone && target.position === position,
        [styles.miss]: animationDone && target.position !== endPosition && target.position === position,
        [styles.normal]: !animationDone || (animationDone && position !== target.position),
    })

    // Первые скобки - показывают цель во время выбора
    // Вторые скобки - показывают уже выбранную цель, если ее позиция не совпадает с мячом
    const showTarget = (showTargetState && canSelectTarget) || ( target.selected && target.position === position && !ball) 

    return (
        <div
            className={containerClass}
            onClick={handleSelectTarget}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseEnter}
            id={`side${position}`}
        >
            { ball && !showTarget &&  <Ball initFieldCallback={initFieldCallback} /> }
            {/* eslint-disable-next-line @next/next/no-img-element */}
            { showTarget && <img src='/img/target.svg' className={styles.target} alt='' /> }
        </div>
    )
}
