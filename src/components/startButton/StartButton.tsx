import { useState, useEffect } from 'react'
import { useAppSelector, useAppDispatch } from '../../state/hooks'
import { startButtonShouldInitialize as startButtonShouldInitializeAction } from '../../state/actions'

import styles from './StartButton.module.scss'

interface iStartButton {
    showHidePlanksCallback: () => void
}

export const StartButton: React.FC<iStartButton> = ({ showHidePlanksCallback }) => {
    const [pressed, setPressed] = useState(false)
    const startButtonShouldInitialize = useAppSelector(state => state.field.startButtonShouldInitialize)
    const dispatch = useAppDispatch()

    const handleMouseUpDown = (e: React.MouseEvent) => {
        if (e.type === 'mousedown') {
            setPressed(true)
        }
        if (e.type === 'mouseup') {
            setPressed(false)
        }
    }

    const handleMouseLeave = () => {
        setPressed(false)
    }

    const handleClick = () => {
        const button = document.getElementById('startButton')
        const text = button?.innerText
        if (text === 'Start') {
            button!.innerText = '3'
        }

        const interval = setInterval(() => {
            const button = document.getElementById('startButton')
            const text = button?.innerText
            switch (text) {
                case '3': {
                    button!.innerText = '2'
                    break
                }
                case '2': {
                    button!.innerText = '1'
                    break
                }
                case '1': {
                    button!.innerText = 'Go!'
                    // Показываем и убираем планки
                    clearInterval(interval)
                    // TODO: это можно сделать через redux
                    showHidePlanksCallback()
                    break
                }
                default: {
                    break
                }
            }
        }, 1000)
    }

    useEffect(() => {
        if (startButtonShouldInitialize) {
            const button = document.getElementById('startButton')
            if (button) {
                button.innerText = 'Start'
            }
            dispatch(startButtonShouldInitializeAction(false))
        }
    })

    return (
        <div
            className={ `${styles.container} ${pressed ? styles.pressed : ''}` }
            onMouseDown={handleMouseUpDown}
            onMouseUp={handleMouseUpDown}
            onMouseLeave={handleMouseLeave}
            onClick={handleClick}
        >
            <div className={styles.innerContainer} id='startButton'>
                {'Start'}
            </div>
        </div>
    )
}
