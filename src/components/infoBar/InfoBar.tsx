import styles from './InfoBar.module.scss'

interface iInfoBar {
    level: number
    score: number
}

export const InfoBar: React.FC<iInfoBar> = ({ level, score }) => {
    return (
        <div className={styles.container}>
            <div className={styles.level}>{`Level: ${level}`}</div>
            <div className={styles.score}>{`Score: ${score}`}</div>
        </div>
    )
}
