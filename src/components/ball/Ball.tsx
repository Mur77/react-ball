import { useEffect, useRef } from 'react'
import { useAppDispatch, useAppSelector } from '../../state/hooks'
import { 
    animationDone, 
    setLevel, 
    setScore, 
    setStep, 
    animationStart as animationStartAction,
    targetSelected,
} from '../../state/actions'
import { getCoordinatesForAnimation, resolveSideDirection } from '../../utils'
import { LEVELS, directionResolve } from '../../constants'

import { iBallCoordinates, tDirection } from '../../interfaces'

import styles from './Ball.module.scss'

interface iAnimationStep {
    x: number // Целевое X
    y: number // Целевое Y
    steps: number // Количество шагов до цели
    direction: tDirection // Направление до цели
}

interface iBall {
    initFieldCallback: () => void
}

export const Ball: React.FC<iBall> = ({ initFieldCallback }) => {
    const ballRef = useRef<HTMLImageElement>(null)
    const ballAudioRef0 = useRef<HTMLAudioElement | null>(null)
    const ballAudioRef1 = useRef<HTMLAudioElement | null>(null)
    const hitAudioRef = useRef<HTMLAudioElement | null>(null)
    const missAudioRef = useRef<HTMLAudioElement | null>(null)

    const startPosition = useAppSelector(state => state.field.startPosition)
    const endPosition = useAppSelector(state => state.field.endPosition)
    const planks = useAppSelector(state => state.field.planks)
    const zeroSideCoordinates = useAppSelector(state => state.field.zeroSideCoordinates)
    const animationStart = useAppSelector(state => state.app.animationStart)
    const showBall = useAppSelector(state => state.field.showBall)
    const dimentions = useAppSelector(state => state.field.dimentions)
    const target = useAppSelector(state => state.field.target)
    const level = useAppSelector(state => state.app.level)
    const score = useAppSelector(state => state.app.score)
    const levelSteps = useAppSelector(state => state.app.steps)

    const dispatch = useAppDispatch()

    const ballCoordinates: iBallCoordinates = { x: 0, y: 0 }
    const animationPath: iAnimationStep[] = []
    const ballPosition: iBallCoordinates = { x: 0, y: 0 }

    let ballAudioOrder = 0

    /**
     * Рассчет траектории мяча
     */
    const calculateBallAnimation = () => {
        // Получаем координаты нулевого шага
        let { x, y } = getCoordinatesForAnimation(startPosition, 'side')
        // Направление от нулевого шага к следующему
        let direction = resolveSideDirection(startPosition, dimentions)
    //console.log('start: x', x, 'y', y, 'direction', direction)

        // Цикл по планкам
        planks.forEach((item) => {
            // Координаты следующей планки
            const { x: newX, y: newY } = getCoordinatesForAnimation({ x: item.y, y: item.x }, 'field', zeroSideCoordinates)
        //console.log('Next plank coords', newX, newY);

            const difference: iBallCoordinates = { x: Math.abs(newX - x), y: Math.abs(newY - y) }
            const steps = (difference.x > difference.y) ? difference.x : difference.y

            animationPath.push({ x: newX, y: newY, steps, direction })
        //console.log('plank: x', newX, 'y', newY, 'direction', direction)

            // Сохраняем координаты
            x = newX, y = newY
            // Новое направление
            direction = item.newDirection
        })

        // Последний шаг
        const { x: newX, y: newY } = getCoordinatesForAnimation(endPosition, 'side')
        const difference: iBallCoordinates = { x: Math.abs(newX - x), y: Math.abs(newY - y) }
        const steps = (difference.x > difference.y) ? difference.x : difference.y

        animationPath.push({ x: newX, y: newY, steps, direction })
    //console.log('end: x', newX, 'y', newY, 'direction', direction)
    //console.log('animationPath', animationPath);
    }

    /**
     * Анимация мяча
     */
    let path = 0, step = 0, steps: number, normalizer = 0
    let animationID: number | null = null
    const speed = 4
    let addX = 0, addY = 0

    const ballAnimation = () => {
//console.log('Animation in progress');

        // Следующий шаг; прибавляем speed для ускорения
        step = step + speed
    //console.log('step', step, 'steps', steps, 'normalizer', normalizer);

        // Нормализуем step, если сумма сделанных шагов больше нужного количества шагов
        // Это может быть из-за того, что мы делаем шаг больше 1
        if (step > steps) {
            normalizer = step - steps
        }

        // Новая позиция мяча
        // Умножаем на speed для ускорения
        addX = directionResolve[animationPath[path].direction][0] * speed
        addY = directionResolve[animationPath[path].direction][1] * speed
    //console.log('addX', addX, 'addY', addY);

        // Нормализация
        if (normalizer > 0) {
            addX = addX - directionResolve[animationPath[path].direction][0] * normalizer
            addY = addY - directionResolve[animationPath[path].direction][1] * normalizer
        }
    //console.log('addX', addX, 'addY', addY);

        ballPosition.x = ballPosition.x + addX
        ballPosition.y = ballPosition.y + addY
    //console.log('ballPosition.x', ballPosition.x, 'ballPosition.y', ballPosition.y);

        // Применяем новую позицию к мячу
        ballRef.current!.style.top = `${ballPosition.y}px`
        ballRef.current!.style.left = `${ballPosition.x}px`

        addX = 0, addY = 0, normalizer = 0

        if (step >= steps) {
            //console.log('step', step, 'steps', steps, 'normalizer', normalizer);
            // Если текущий отрезок пройден, переходим на следующий
            path = path+1
            if (path >= animationPath.length) {
                // Если отрезки закончились, завершаем анимацию
                if (animationID) {
                    cancelAnimationFrame(animationID)
                    console.clear()
                    dispatch(animationDone(true))
                    dispatch(animationStartAction(false))
                    if (target.position === endPosition) {
                        hitAudioRef.current!.play()
                        // Убираем цель, чтобы не было наложения мяча и цели
                        dispatch(targetSelected({ position: endPosition, selected: false }))
                        // Через 3 сек посылаем событие 
                        setTimeout(() => {
                            gameWon()
                        }, 3000)
                    } else {
                        missAudioRef.current!.play()
                        setTimeout(() => {
                            gameLoose()
                        }, 3000)
                    }
                    return
                }
            }  else {
                // Звук удара о планку
                // Запускаем по очереди, т.к. если один - он иногда не успевает
                if (ballAudioOrder === 0) {
                    ballAudioRef0.current!.play()
                    ballAudioOrder = 1
                } else {
                    ballAudioRef1.current!.play()
                    ballAudioOrder = 0
                }
            }
            step = 0
            steps = animationPath[path].steps
        }

        animationID = requestAnimationFrame(ballAnimation)
    }

    const gameWon = () => {
//console.log('Game won');
        const newScore = score+LEVELS[level].score

        dispatch(setScore(newScore))
//console.log('Score', newScore);
        
        localStorage.setItem('reactBall.score', String(newScore))
        // Каждый уровень повторяем 2 раза
        if (levelSteps+1 < 2) {
            dispatch(setStep(levelSteps+1))
        } else {
            dispatch(setStep(0))
            if (level+1 === LEVELS.length) {
                localStorage.setItem('reactBall.level', '0')
                dispatch(setLevel(0))
            } else {
                localStorage.setItem('reactBall.level', String(level+1))
                dispatch(setLevel(level+1))
            }

        }
        initFieldCallback()
    }

    const gameLoose = () => {
//console.log('Game loose');
        initFieldCallback()
    }

    useEffect(() => {
//console.log('Ball effect', animationStart);

        // Начальные координаты мяча
        ballCoordinates.x = ballRef.current!.x
        ballCoordinates.y = ballRef.current!.y

        ballAudioRef0.current = new Audio('/sound/ball.wav')
        ballAudioRef1.current = new Audio('/sound/ball.wav')
        hitAudioRef.current = new Audio('/sound/hit.wav')
        missAudioRef.current = new Audio('/sound/miss.wav')

        if (animationStart) {
            calculateBallAnimation()
            // eslint-disable-next-line react-hooks/exhaustive-deps
            steps = animationPath[path].steps
            ballAnimation()
        }
    }, [animationStart])

    return (
        <>
            { /* eslint-disable-next-line @next/next/no-img-element */ }
            <img
                src="/img/ball.svg"
                className={`${showBall ? styles.ball : styles.noBall}`}
                alt=''
                ref={ballRef}
            />
        </>
    )
}
