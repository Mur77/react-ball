import { useEffect, useRef } from 'react'

import { useAppSelector, useAppDispatch } from '../../state/hooks'
import { Cell } from '../cell/Cell'
import { Side } from '../side/Side'
import { StartButton } from '../startButton/StartButton'
import { ResetLevelButton } from '../resetLevelButton/ResetLevelButton'
import { InfoBar } from '../infoBar/InfoBar'
import {
    animationStart,
    animationDone,
    canSelectTargetAction,
    initField,
    setLevel,
    showBall,
    showPlanks,
    sidesShouldInitialize,
    targetSelected,
    setScore,
    initStartEndPosition
} from '../../state/actions'
import { LEVELS } from '../../constants'

import { iCell } from '../../interfaces'

import styles from './Field.module.scss'

export const Field = () => {
    const field = useAppSelector(state => state.field.field)
    const dimentions = useAppSelector(state => state.field.dimentions)
    const startPosition = useAppSelector(state => state.field.startPosition)
    const level = useAppSelector(state => state.app.level)
    const score = useAppSelector(state => state.app.score)
    const dispatch = useAppDispatch()

    const appearRef = useRef<HTMLAudioElement>(null)
    const disappearRef = useRef<HTMLAudioElement>(null)

//console.log('startPosition', startPosition);

    const initFieldWrapper = (noShowPlanks?: boolean) => {
        //console.log('level', level, 'dimentions', LEVELS[level].dimentions);
        
        dispatch(targetSelected( { position: 0, selected: false }))
        dispatch(animationStart(false))
        dispatch(animationDone(false))
        dispatch(showPlanks(false))
        dispatch(canSelectTargetAction(false))
        dispatch(initStartEndPosition())
        dispatch(sidesShouldInitialize(true)) // Признак инициализации сторон, чтобы сбросить флаг показывания цели
        dispatch(initField())

        if (!noShowPlanks) {
            showHidePlanks()
        }
    }

    const showHidePlanks = () => {
        const show = () => {
            appearRef.current!.play()
            dispatch(showPlanks(true))
            dispatch(showBall(true))
        }
    
        const hide = () => {
            disappearRef.current!.play()
            dispatch(showPlanks(false))
            dispatch(canSelectTargetAction(true))
        }

        // Показываем и через timeout сек убираем планки
        show()
        setTimeout(() => {
            hide()
        }, LEVELS[level].timeout)
    }

    const resetLevel = () => {
        dispatch(setScore(0))
        dispatch(setLevel(0))

        initFieldWrapper(true)
    }

    useEffect(() => {
        // Получаем текущий уровень из localStorage
        let storedLevel = Number(localStorage.getItem('reactBall.level'))
        let storedScore = Number(localStorage.getItem('reactBall.score'))
        
        if ( storedLevel === NaN || storedLevel < 0 || storedLevel >= LEVELS.length ) {
            storedLevel = 0
        }
        if ( storedScore === NaN || storedScore < 0 ) {
            storedScore = 0
        }
        dispatch(setLevel(storedLevel))
        dispatch(setScore(storedScore))
        dispatch(initField())
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className={styles.container}>
            <div className={styles.topBalls}>
                {Array.from({length: dimentions}, (item, index) => index).map((item, index) => (
                    <Side 
                        key={index} 
                        position={index} 
                        ball={startPosition === index}
                        initFieldCallback={initFieldWrapper} 
                    />
                ))}
            </div>
            <div className={styles.field}>
                {field.map((item: iCell[], index: number) => {
                    const leftPosition = dimentions*4-index-1; // Номер ячейки стороны слева
                    const rightPosition = index+dimentions; // Номер ячейки стороны справа
                    
                    return (
                        <div key={index} className={styles.line}>
                            <div className={styles.leftBalls}>
                                <Side 
                                    position={leftPosition} 
                                    ball={startPosition === leftPosition}
                                    initFieldCallback={initFieldWrapper} 
                                />
                            </div>
                            {item.map((item1, index1: number) => (
                                <Cell key={String(index)+index1} { ...item1 } />
                            ))}
                            <div className={styles.rightBalls}>
                                <Side 
                                    position={rightPosition} 
                                    ball={startPosition === rightPosition} 
                                    initFieldCallback={initFieldWrapper}
                                />
                            </div>
                        </div>
                    )
                })}
            </div>
            <div className={styles.bottomBalls}>
                {Array.from({length: dimentions}, () => 0).map((item, index) => {
                    const bottomPosition = dimentions*3-index-1
                    return (
                        <Side 
                            key={index} 
                            position={bottomPosition} 
                            ball={startPosition === bottomPosition}
                            initFieldCallback={initFieldWrapper}
                        />
                    )
                })}
            </div>
            <div className={styles.buttonsContainer}>
                <StartButton showHidePlanksCallback={showHidePlanks}/>
                <ResetLevelButton clickCallback={resetLevel} />
            </div>
            <div className={styles.infoContainer}>
                <InfoBar level={level} score={score} />
            </div>
            <audio src='/sound/appear.mp3' ref={appearRef} />
            <audio src='/sound/disappear.mp3' ref={disappearRef} />
        </div>
    )
}
