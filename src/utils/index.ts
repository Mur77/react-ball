import { iBallCoordinates, iCell, tDimentions, tDirection, tLocation } from '../interfaces'


export const getStartPosition = (max: number): number => {
    return random(0, max)
}


// get Random value between min and max (inclusive)
export const random = (min: number, max: number): number => {

    return Math.floor(Math.random() * (max - min + 1) + min)
}


export const getRandomDirection = (currentDirection: tDirection): tDirection => {
    const value = random(0, 1)

    const horizontal: tDirection[] = ['left', 'right']
    const vertical: tDirection[] = ['up', 'down']

    if (currentDirection === 'up' || currentDirection === 'down') {
        return horizontal[value]
    } else {
        return vertical[value]
    }
}


export const resolveSideDirection = (sideCell: number, dimentions: number): tDirection => {
    const directions: tDirection[] = ['down', 'left', 'up', 'right']

    return directions[Math.floor(sideCell / dimentions)]
}


export const getCoordinatesForAnimation = (
    coord: iBallCoordinates | number, // Координаты поля или номер ячейки стороны
    location: tLocation, 
    zeroSideCoordinates?: iBallCoordinates,
): iBallCoordinates => {
    const ballOffset = 13

    if ( location === 'field' ) {
        // const cellSize = 50 // Размер ячейки, px
        if (typeof(coord) !== 'number') {
            const cell = document.getElementById(getCellId(coord.x, coord.y))
            return {
                // x: coord.x * cellSize + zeroSideCoordinates!.x + ballOffset,
                // y: coord.y * cellSize + zeroSideCoordinates!.y + ballOffset,
                x: cell!.offsetLeft + ballOffset,
                y: cell!.offsetTop + ballOffset,
            }
        }
    }

    const cell = document.getElementById(`side${coord}`)
    return {
        x: cell!.offsetLeft + ballOffset,
        y: cell!.offsetTop + ballOffset,
    }

}


export const getIJByPosition = (position: number, dimentions: tDimentions) => {
    // Определяем положение в массиве по position
    const i = Math.trunc(position / dimentions)
    const j = position - i*dimentions

    return [i, j]
}


export const getReverseDirection = (direction: tDirection): tDirection => {
    const reverseDirectionResolver: { [key: string]: tDirection } = {
        'up': 'down',
        'down': 'up',
        'right': 'left',
        'left': 'right',
    }

    return reverseDirectionResolver[direction]
}


export const logPlankArray = (plankArray: iCell[][]): void => {
    console.log('PlankArray ------------');

    // Верхняя строка с номерами столбцов
    const line = ['  ']
    for(let i=0; i<plankArray.length; i++) {
        const num = String(i)
        if (num.length < 2) {
            line.push(' ' + num)
        } else {
            line.push(num)
        }
    }
    console.log(line);

    // Поле
    for(let i=0; i<plankArray.length; i++) {
        const line = []

        // Номер строки
        const num = String(i)
        if (num.length < 2) {
            line.push(' ' + num)
        } else {
            line.push(num)
        }

        for(let j=0; j<plankArray[i].length; j++) {
            let cell = ''

            if (plankArray[i][j].occupied) {
                cell = cell+'o'
            } else {
                cell = cell+' '
            }

            if (plankArray[i][j].plank === '') {
                cell = cell+' '
            } else if (plankArray[i][j].plank === '\\') {
                cell = cell+'\\'
            } else {
                cell = cell+"/"
            }

            line.push(cell)
        }
        console.log(line);
        
    }
}


export const getCellId = (x: number, y: number): string => `cellx${y}y${x}`
