import { AnyAction } from 'redux'

import { store } from './store'
import { 
    INIT_FIELD, 
    TARGET_SELECTED,
    SET_GAME_IN_PROGRESS,
    SET_GAME_WON,
    SET_LEVEL,
    ANIMATION_START,
    SHOW_PLANKS,
    SHOW_BALL,
    ANIMATION_DONE,
    SHOW_TARGET,
    CAN_SELECT_TARGET,
    SET_SCORE,
    SET_STEP,
    SET_FIELD_PARAMS,
    SIDES_SHOULD_INITIALIZE,
    START_BUTTON_SHOULD_INITIALIZE,
    INIT_START_END_POSITION,
} from './constants'
import { LEVELS } from '../constants'

import { tDimentions, iTarget } from '../interfaces'

export function initField(): AnyAction {
    //store.dispatch(setGameWon(false))
    return { type: INIT_FIELD }
}

export function setGameInProgress(status: boolean): AnyAction {
    return { type: SET_GAME_IN_PROGRESS, payload: status }
}

export function setGameWon(status: boolean): AnyAction {
    return { type: SET_GAME_WON, payload: status }
}

export function setLevel(level: number) {
    store.dispatch(setFieldParams({ dimentions: LEVELS[level].dimentions, plankCount: LEVELS[level].plankCount }))
    return { type: SET_LEVEL, payload: level }
}

export function targetSelected(target: iTarget): AnyAction {
    return { type: TARGET_SELECTED, payload: target }
}

export function animationStart(mode: boolean): AnyAction {
    return { type: ANIMATION_START, payload: mode }
}

export function showPlanks(visible: boolean): AnyAction {
    return { type: SHOW_PLANKS, payload: visible }
}

export function showBall(visible: boolean): AnyAction {
    return { type: SHOW_BALL, payload: visible }
}

export function animationDone(mode: boolean): AnyAction {
    return { type: ANIMATION_DONE, payload: mode }
}

export function showTargetAction(position: number, visible: boolean): AnyAction {
    return { type: SHOW_TARGET, payload: { position, visible } }
}

export function canSelectTargetAction(mode: boolean): AnyAction {
    return { type: CAN_SELECT_TARGET, payload: mode }
}

export function setScore(score: number): AnyAction {
    return { type: SET_SCORE, payload: score }
}

export function setStep(step: number): AnyAction {
    return { type: SET_STEP, payload: step }
}

export function setFieldParams(params: { dimentions: tDimentions, plankCount: number }): AnyAction {
    return { type: SET_FIELD_PARAMS, payload: params }
}

export function sidesShouldInitialize(flag: boolean): AnyAction {
    return { type: SIDES_SHOULD_INITIALIZE, payload: flag }
}

export function startButtonShouldInitialize(flag: boolean): AnyAction {
    return { type: START_BUTTON_SHOULD_INITIALIZE, payload: flag }
}

export function initStartEndPosition(): AnyAction {
    return { type: INIT_START_END_POSITION }
}
