import { AnyAction } from "redux"

import { tLevel } from "../interfaces"

import { 
    SET_GAME_IN_PROGRESS, 
    SET_GAME_WON, 
    SET_LEVEL, 
    ANIMATION_START, 
    ANIMATION_DONE, 
    CAN_SELECT_TARGET,
    SET_SCORE,
    SET_STEP,
} from './constants'

interface appState {
    // dimentions: [number, number],
    gameInProgress: boolean,
    gameWon: boolean,
    timer: number,
    level: number
    animationStart: boolean
    animationDone: boolean
    canSelectTarget: boolean
    score: number
    steps: number // Сколько раз пройден уровень level
}

const initialState: appState = {
    gameInProgress: false,
    gameWon: false,
    timer: 0,
    level: 0,
    score: 0,
    steps: 0,
    animationStart: false,
    animationDone: false,
    canSelectTarget: false,
}

export const appReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case SET_GAME_IN_PROGRESS: return { ...state, gameInProgress: action.payload }
        case SET_GAME_WON: return { ...state, gameWon: action.payload }
        case SET_LEVEL: return { ...state, level: action.payload }
        case ANIMATION_START: return { ...state, animationStart: action.payload }
        case ANIMATION_DONE: return { ...state, animationDone: action.payload }
        case CAN_SELECT_TARGET: return { ...state, canSelectTarget: action.payload }
        case SET_SCORE: return { ...state, score: action.payload }
        case SET_STEP: return { ...state, steps: action.payload }
        default: return state
    }
}
