import { combineReducers } from 'redux'

import { fieldReducer } from './fieldReducer'
import { appReducer } from './appReducer'

export const rootReducer = combineReducers({
    field: fieldReducer,
    app: appReducer,
})
