import { AnyAction } from 'redux'

import { getIJByPosition, getRandomDirection, getReverseDirection, getStartPosition, logPlankArray, random } from '../utils'
import {
    INIT_FIELD,
    INIT_START_END_POSITION,
    SET_FIELD_PARAMS,
    SHOW_BALL,
    SHOW_PLANKS,
    SHOW_TARGET,
    SIDES_SHOULD_INITIALIZE,
    START_BUTTON_SHOULD_INITIALIZE,
    TARGET_SELECTED
} from './constants'

import { tDimentions, iCell, iSide, tPosition, tDirection, iCellCoordinate, tPlank, iField, iPlank } from '../interfaces'
import { directionResolve } from '../constants'

type sideT = [iSide[], iSide[], iSide[], iSide[]]

// --- Инициализация поля ---
export const initField = (dimentions: tDimentions, plankCount: number): [
    iCell[][], sideT, iPlank[], number, number
] => {
    const plankArray = new Array<iCell[]>(dimentions)
    const side: sideT = [[], [], [], []]
    const startPosition = getStartPosition(dimentions*4-1)
    let endPosition = 0
    const planks: iPlank[] = []
    let collisionFlag = true

    // Цикл нужен для возможности перезапуска инициализации поля в случае пересечений планок
    while (collisionFlag) {
    //console.log('Init start');
        collisionFlag = false
        let counter = 0 // Порядковый номер ячейки
        planks.splice(0, planks.length)
        // --- Инициализация поля
        for (let i=0; i<dimentions; i++) {
            plankArray[i] = new Array<iCell>(dimentions)
            for (let j=0; j<dimentions; j++) {
                plankArray[i][j] = { plank: '', x: i, y: j, orderNumber: counter, occupied: false }
                counter = counter + 1
            }
        }

        // --- Инициализация сторон
        interface iCalc {
            x: number   // приращение x
            y: number   // приращение y
            dimention: number   // направление - x или y, для вычисления номера линии
            position: tPosition  // какая сторона поля
            direction: tDirection // Какое направление от этой точки в сторону поля
        }

        const calc: iCalc[] = [
            {x: 1, y: 0, dimention: 1, position: 'top', direction: 'down'}, 
            {x: 0, y: 1, dimention: 0, position: 'right', direction: 'left'}, 
            {x: -1, y: 0, dimention: 1, position: 'bottom', direction: 'up'}, 
            {x: 0, y: -1, dimention: 0, position: 'left', direction: 'right'}
        ]

        counter = 0 // Порядковый номер стороны (position)
        let x = 0, y = 0

        for (let i=0; i<4; i++) {
            for (let j=0; j<dimentions; j++) {
                const sideElement: iSide = { x, y, number: 0, direction: 'up', line: 0, position: 'top', showTarget: false }

                sideElement.x = x
                sideElement.y = y
                sideElement.number = counter
                sideElement.direction = calc[i].direction
                sideElement.position = calc[i].position
                counter = counter+1

                // Вычисляем номер линии
                if (calc[i].dimention === 0) {
                    sideElement.line = y
                } else {
                    sideElement.line = x
                }

                // Координаты следующей ячейки
                if (j < dimentions-1) {
                    x = x+calc[i].x
                    y = y+calc[i].y
                }

                side[i].push(sideElement)
            }
        }

        // --- Инициализация досок
        // --- Алгоритм для квадратного поля !
        let currentDirection: tDirection // Текущее направление мяча
        let newDirection: tDirection = 'up' // Новое направление мяча
        let currentLine // Текущая линия (горизонтальная или вертикальная)
        let newPlank: tPlank = '' // Для вычисленной новой планки
        let randomLineElement // Случайное положение новой планки
        let randomStart = 0, randomEnd = dimentions-1 // В каких пределах генерируем случайное число для расположения планки на линии
        let reverseDirection: tDirection = 'up' // Для вычисления обратного направления
        let testX, testY, prevPlankX: number = 0, prevPlankY: number = 0, prevLine: number = 0 // Для проверки пересечения планок

        counter = 0 // Счетчик планок

        // 1. Выбираем стартовую позицию (в начале ф-и)
        // y = | a / dimention |
        // x = a - y * dimention, где a - номер стартовой позиции (startPosition)
        const startPositionInSideArray: iCellCoordinate = { x: 0, y: 0 }

        startPositionInSideArray.y = Math.trunc(startPosition / dimentions)
        startPositionInSideArray.x = startPosition - (startPositionInSideArray.y * dimentions)

        // 1a. Текущее направление
        currentDirection = side[startPositionInSideArray.y][startPositionInSideArray.x].direction
        currentLine = side[startPositionInSideArray.y][startPositionInSideArray.x].line

    //console.log(startPosition, startPositionInSideArray, currentDirection, currentLine);

        do {
            //console.log('-------- Iteration ---------');
            
            for (let i=0; i<dimentions*2; i++) {
                // 2. Выбираем случайную ячейку в строке / столбце
                // Если она попадет на уже занятую ячейку (occupied), выбираем снова (max dimentions*2 раз)
                randomLineElement = random(randomStart, randomEnd)
        //console.log('randomStart', randomStart, 'randomEnd', randomEnd, 'randomLineElement', randomLineElement)

                // 3. Получаем новое случайное направление
                newDirection = getRandomDirection(currentDirection)
        //console.log(randomLineElement, newDirection);

                // 4. Проверяем, не будет ли выхода из поля при текущем направлении
                if ( (newDirection === 'left' && currentLine === 0) ||
                    (newDirection === 'right' && currentLine === dimentions-1) ||
                    (newDirection === 'up' && currentLine === 0) ||
                    (newDirection === 'down' && currentLine === dimentions-1) ) {
        //console.log('Выход из поля, counter = ', counter, 'plankCount = ', plankCount)
                        // 4a. Если выход из поля будет, и это не последняя планка, меняем направление на обратное
                        if (counter < plankCount-1) {
                            switch (newDirection) {
                                case 'up': {
                                    newDirection = 'down'
                                    break
                                }
                                case 'down': {
                                    newDirection = 'up'
                                    break
                                }
                                case 'left': {
                                    newDirection = 'right'
                                    break
                                }
                                case 'right': {
                                    newDirection = 'left'
                                    break
                                }
                                default: {
                                    break
                                }
                            }
                        }
                }
        //console.log('currentDirection', currentDirection, 'newDirection', newDirection);

                prevLine = currentLine

                // 5. Вычисляем какая нужна планка и ee координаты
                switch (currentDirection) {
                    case 'up': {
                        if (newDirection === 'left') newPlank = '\\'; else newPlank = '/';
                        x = currentLine
                        y = randomLineElement
                        currentLine = y // Переключаем текущую линию
                        break
                    }
                    case 'down': {
                        if (newDirection === 'left') newPlank = '/'; else newPlank = '\\';
                        x = currentLine
                        y = randomLineElement
                        currentLine = y
                        break
                    }
                    case 'left': {
                        if (newDirection === 'up') newPlank = '\\'; else newPlank = '/';
                        x = randomLineElement
                        y = currentLine
                        currentLine = x
                        break
                    }
                    case 'right': {
                        if (newDirection === 'up') newPlank = '/'; else newPlank = '\\';
                        x = randomLineElement
                        y = currentLine
                        currentLine = x
                        break
                    }
                    default: {
                        break
                    }
                }
        //console.log('x', x, 'y', y);

                //console.log('currentLine', currentLine);

                if (!plankArray[y][x].occupied) {
                    break
                }

                if (i < dimentions*2-1) {
                    // Если цикл еще не заканчивается, возвращаем currentLine
                    currentLine = prevLine
                } else {
                    //console.log('Cant find free cell!');
                    collisionFlag = true
                }

                //console.log('[', x, ',', y, '] occupied!', 'currentLine', currentLine);
            }

            // Ставим планку
            plankArray[y][x].plank = newPlank
            plankArray[y][x].occupied = true

            // 5z. Проверяем, нет ли планок между предыдущей и текущей (новой)
            // Обратное направление
            reverseDirection = getReverseDirection(currentDirection)
            //const stopTestPosition = (reverseDirection === 'up' || reverseDirection === 'left') ? 0 : dimentions-1
            //logPlankArray(plankArray)
        
        //console.log('reverseDirection' ,reverseDirection, 'startPosition', startPosition, 'currentLine', currentLine)
        //console.log(JSON.parse(JSON.stringify(planks)));
        //console.log('Current plank: [', x, ', ', y, ']');
        //console.log('directionResolve [', directionResolve[reverseDirection][0], ', ', directionResolve[reverseDirection][1], ']');
        //console.log('prevLine', prevLine);
        

            testX = x, testY = y

            // Координаты предыдущей планки
            if (planks.length === 0) {
                // Вычисляем координаты первой ячейки перед стороной-целью
                switch (currentDirection) {
                    case 'up': {
                        prevPlankX = prevLine
                        prevPlankY = dimentions-1
                        break
                    }
                    case 'down': {
                        prevPlankX = prevLine
                        prevPlankY = 0
                        break
                    }
                    case 'left': {
                        prevPlankX = dimentions-1
                        prevPlankY = prevLine
                        break
                    }
                    case 'right': {
                        prevPlankX = 0
                        prevPlankY = prevLine
                        break
                    }
                    default: {
                        break
                    }
                }
            } else {
                prevPlankX = planks[planks.length-1].x
                prevPlankY = planks[planks.length-1].y             
            }

            // Помечаем крайнюю ячейку, т.к. она не войдет в цикл
            plankArray[prevPlankY][prevPlankX].occupied = true

            //console.log('prev plank: [', prevPlankX, ', ', prevPlankY, ']');

            // Проверяем промежуток между предыдущей и текущей планками на наличие других планок
            while ( !(testX === prevPlankX && testY === prevPlankY) ) {

                //console.log('testX', testX, 'testY', testY);

                if ( !(testX === x && testY === y) && plankArray[testY][testX].plank !== '') {
                    //console.log('Collision!');
                    collisionFlag = true
                }
                
                // Помечаем пройденную ячейку занятой, на ней нельзя будет ставить новые планки
                plankArray[testY][testX].occupied = true

                testX = testX + directionResolve[reverseDirection][0]
                testY = testY + directionResolve[reverseDirection][1]

            }

        //console.log('Store new plank');
        
            // Сохраняем планку
            planks.push({ x, y, newDirection })
            counter = counter + 1

        //console.log('Calculate new plank boundaries');
        
            // 5a. Вычисляем диапазон, в котором будем генерировать следующую случайную позицию для планки,
            // чтобы она не вышла за линию следования мяча
            switch (newDirection) {
                case 'up': {
                    randomStart = 0
                    randomEnd = y-1
                    break
                }
                case 'down': {
                    randomStart = y+1
                    randomEnd = dimentions-1
                    break
                }
                case 'left': {
                    randomStart = 0
                    randomEnd = x-1
                    break
                }
                case 'right': {
                    randomStart = x+1
                    randomEnd = dimentions-1
                    break
                }
                default: {
                    break
                }
            }

            // 5b.
            currentDirection = newDirection
    //console.log('newPlank', newPlank, 'counter', counter, 'plankCount', plankCount, 'x', x, 'y', y);
    //console.log('randomStart', randomStart, 'randomEnd', randomEnd);

        //console.log('End of iteration');
        
            // 6
        } while (counter < plankCount)

        // 7. Вычисляем конечную позицию
        // 7a. Получаем направление, обратное currentDirection
        reverseDirection = getReverseDirection(currentDirection)

        // 7b. Находим ячейку с line === currentLine и direction = reverseDirection
        let found = false
        for (let i=0; i<4; i++) {
            for (let j=0; j<dimentions; j++) {
                const sideElement = side[i][j]
                if ( sideElement.direction ===  reverseDirection && sideElement.line === currentLine ) {
                    endPosition = sideElement.number
                    found = true
                    break
                }
            }
            if (found) {
                break
            }
        }

        // 7c. Проверяем, нет ли планок между предыдущей и текущей (новой)
        const stopTestPosition = (reverseDirection === 'up' || reverseDirection === 'left') ? 0 : dimentions-1
        //logPlankArray(plankArray)
        //console.log('currentDirection', currentDirection, 'currentLine', currentLine);
        
        // Вычисляем координаты последней ячейки перед стороной-целью
        switch (currentDirection) {
            case 'up': {
                x = currentLine
                y = 0
                break
            }
            case 'down': {
                x = currentLine
                y = dimentions-1
                break
            }
            case 'left': {
                x = 0
                y = currentLine
                break
            }
            case 'right': {
                x = dimentions-1
                y = currentLine
                break
            }
            default: {
                break
            }
        }

    //console.log('reverseDirection' ,reverseDirection, 'stopTestPosition', stopTestPosition, 'endPosition', endPosition)
    //console.log(JSON.parse(JSON.stringify(planks)));
    //console.log('Current plank: [', x, ', ', y, ']');
    //console.log('directionResolve [', directionResolve[reverseDirection][0], ', ', directionResolve[reverseDirection][1], ']');

        testX = x, testY = y

        // Предыдущая планка
        prevPlankX = planks[planks.length-1].x
        prevPlankY = planks[planks.length-1].y             

        //console.log('prev plank: [', prevPlankX, ', ', prevPlankY, ']');

        // Проверяем промежуток между последней планкой и endPosition на наличие других планок
        while ( !(testX === prevPlankX && testY === prevPlankY) ) {

            //console.log('testX', testX, 'testY', testY);

            if ( plankArray[testY][testX].plank !== '') {
                //console.log('Last Collision!');
                collisionFlag = true
            }

            testX = testX + directionResolve[reverseDirection][0]
            testY = testY + directionResolve[reverseDirection][1]

        }

    }

//console.log(endPosition);

    return [plankArray, side, planks, startPosition, endPosition]

}

const initialDimentions: tDimentions = 4
const initialPlankCount = 3
// const [field, side, startPosition, endPosition] = initField(initialDimentions, initialPlankCount)

const initialState: iField = {
    field: [],
    // Если true - требуется инициализация поля
    sidesShouldInitialize: false,
    startButtonShouldInitialize: false,
    // Сколько открыто клеток (для выявления момента когда остались одни мины)
    dimentions: initialDimentions,
    plankCount: initialPlankCount,
    plankOpen: false,
    side: [[],[],[],[]],
    sideArray: [],
    startPosition: 0,
    endPosition: 0,
    target: {
        position: 0,
        selected: false,
    },
    planks: [],
    zeroSideCoordinates: { x: 94, y: 94 },
    showPlanks: false,
    showBall: false,
}

export const fieldReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case INIT_FIELD: {
            const [plankArray, side, planks, startPosition, endPosition] = initField(state.dimentions, state.plankCount)
            return {
                ...state,
                field: plankArray,
                side, 
                planks,
                startPosition,
                endPosition,
                sidesShouldInitialize: false,
            }
        }
        case TARGET_SELECTED: {
            return { ...state, target: action.payload }
        }
        case SHOW_PLANKS: {
            return { ...state, showPlanks: action.payload }
        }
        case SHOW_BALL: {
            return { ...state, showBall: action.payload }
        }
        case SHOW_TARGET: {
            // Копируем массив side
            const newSide: sideT = [[], [], [], []]
            for(let i=0; i<4; i++) {
                newSide[i] = Object.assign([], state.side[i])
            }
            // Определяем положение в массиве по position
            const [i, j] = getIJByPosition(action.payload.position, state.dimentions)

            // Устанавливаем флаг в новом массиве
            newSide[i][j].showTarget = action.payload.visible

            return { ...state, side: newSide }
        }
        case SET_FIELD_PARAMS: {
            return { ...state, dimentions: action.payload.dimentions, plankCount: action.payload.plankCount }
        }
        case SIDES_SHOULD_INITIALIZE: {
            return { ...state, sidesShouldInitialize: action.payload }
        }
        case START_BUTTON_SHOULD_INITIALIZE: {
            return { ...state, startButtonShouldInitialize: action.payload }
        }
        case INIT_START_END_POSITION: {
            return { ...state, startPosition: 0, endPosition: 0 }
        }
        default: return state
    }
}
