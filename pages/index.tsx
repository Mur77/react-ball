import Head from 'next/head'
import Image from 'next/image'

import { Field } from '../src/components/field/Field'

export default function Home() {
    return (
        <div>
            <Head>
                <title>React ball</title>
                <meta name="react ball" content="React ball" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Field />
        </div>
    )
}
